import hashlib, time, binascii
import system, mouse, input_mikrofon
from hashlib import sha512

key_size = 256
numeral_system = "HEX"


def generator(key_size,numeral_system):
    entropy1 = system.system()
    entropy2 = mouse.mouse()
    entropy3 = input_mikrofon.mikrofon()

    if key_size == 56:
        entropy = sha512((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:14]
    elif key_size == 168:
        entropy = sha512((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:42]
    elif key_size == 128:
        entropy = sha512((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:32]
    elif key_size == 192:
        entropy = sha512((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:48]
    else:
        entropy = sha512((entropy1 + entropy2 + entropy3).encode()).hexdigest()[:64]

    if numeral_system == "BIN":
        #key = format(int(key,16), '0%db'%key_size)         #binární 
        key = bin(int(entropy,16))[2:].zfill(key_size)
        return str(key),entropy                             #vracime klic i jeho hex typ
    elif numeral_system == "DEC":
        key = int(entropy,16)                               #decimální
        return (str(key),entropy)
    else:
        return entropy,entropy
        
        



"""
startSeed = str(binascii.hexlify(hashlib.sha512(entropy.encode('ascii')).digest()))[2:-1]

print("Start seed = SHA-256(entropy) =", startSeed)

min = 10
max = 20
for i in range(10):
    nextSeed = startSeed + '|' + str(i)
    hash = hashlib.sha512(nextSeed.encode('ascii')).digest()
    bigRand = int.from_bytes(hash, 'big')
    rand = min + bigRand % (max - min + 1)
    print(nextSeed, bigRand, '-->', rand)
"""
